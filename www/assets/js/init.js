var page = $('html, body');
var navbar = $('nav');
var navbarOffset = 5;

function initNavbarScroll(){

	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
		var sectionIntro = $('.intro');
		var sectionTeam = $('.team');
		var sectionServices = $('.services');
		var sectionBrands = $('.brands');
		var sectionContact = $('.contact');
		var item = $('li');

		if($(window).scrollTop() >= navbarOffset){
			navbar.addClass('scrolled');
		}else{
			navbar.removeClass('scrolled');
		}

		if(navbar.offset().top < (sectionTeam.offset().top - 120)){
			item.removeClass('active');
			$('[data-scroll=".intro"]').parent().addClass('active');
		}else if((navbar.offset().top >= (sectionTeam.offset().top - 100)) && (navbar.offset().top < sectionServices.offset().top - 100)){
			item.removeClass('active');
			$('[data-scroll=".team"]').parent().addClass('active');
		}else if((navbar.offset().top >= (sectionServices.offset().top - 100)) && (navbar.offset().top < sectionBrands.offset().top - 100)){
			item.removeClass('active');
			$('[data-scroll=".services"]').parent().addClass('active');
		}else if((navbar.offset().top >= (sectionBrands.offset().top - 100)) && (navbar.offset().top < sectionContact.offset().top - 100)){
			item.removeClass('active');
			$('[data-scroll=".brands"]').parent().addClass('active');
		}else if(navbar.offset().top >= sectionContact.offset().top - 100){
			item.removeClass('active');
			$('[data-scroll=".contact"]').parent().addClass('active');
		}

	});
}


function initScrollTo(){
	var button = $('.scroll');

	button.click(function(event){
		event.preventDefault();
		var selectArea = $($(this).data('scroll'));

		page.animate({ scrollTop: selectArea.offset().top - 90 }, 1000);
	});
}


$(document).ready(function(){

	if($(window).scrollTop() >= navbarOffset){
		navbar.addClass('scrolled');
	}else{
		navbar.removeClass('scrolled');
	}

	// Functions init
	initScrollTo();
	initNavbarScroll();

	AOS.init();

});
